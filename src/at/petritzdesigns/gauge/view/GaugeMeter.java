package at.petritzdesigns.gauge.view;

import at.petritzdesigns.gauge.utility.ColorFade;
import at.petritzdesigns.gauge.utility.ColorFade3;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 * BitBotControl
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class GaugeMeter extends JPanel implements MouseListener, MouseMotionListener {

    /**
     * Description label of the gauge
     */
    private String description;

    /**
     * Unit label of the guage
     */
    private String unit;

    /**
     * Minimum value
     */
    private Integer minValue;

    /**
     * Maximal value
     */
    private Integer maxValue;

    /**
     * Current value
     */
    private Integer currentValue;

    /**
     * Padding = size of donut ring
     */
    private Integer padding;

    /**
     * Margin to the panel
     */
    private Integer margin;

    /**
     * Radius of the donut, will be calculated automatically
     */
    private Integer radius;

    /**
     * Text color
     */
    private Color textColor;

    /**
     * Faded color for the foreground
     */
    private ColorFade foregroundColor;

    /**
     * Background color of gauge
     */
    private Color gaugeBackgroundColor;

    /**
     * Background color of the panel
     */
    private Color backgroundColor;

    /**
     * Whether gauge interaction is enabled or not
     */
    private boolean enabled;

    /**
     * Listener if value was changed
     */
    private ActionListener valueChanged;

    /**
     * Default constructor<br>
     * sets default values<br>
     * <ul>
     * <li>Description = Name</li>
     * <li>Unit = %</li>
     * <li>Min Value = 0</li>
     * <li>Max Value = 100</li>
     * <li>Current Value = 50</li>
     * <li>Padding = 75</li>
     * <li>Margin = 50</li>
     * <li>Text color = Black</li>
     * <li>Foreground color = red, yellow, green</li>
     * <li>Background color = default panel color</li>
     * <li>Gauge background color = white</li>
     * <li>Enabled = true</li>
     * </ul>
     */
    public GaugeMeter() {
        this.description = "Name";
        this.unit = "%";
        this.minValue = 0;
        this.maxValue = 100;
        this.currentValue = 0;
        this.padding = 75;
        this.margin = 50;
        this.radius = 0;
        this.textColor = Color.black;
        this.foregroundColor = new ColorFade3(Color.red, Color.yellow, Color.green);
        this.backgroundColor = UIManager.getColor("Panel.background");
        this.gaugeBackgroundColor = Color.white;
        this.enabled = true;

        addMouseListener(this);
        addMouseMotionListener(this);
    }

    /**
     * Description that sets description
     *
     * @param description description
     */
    public GaugeMeter(String description) {
        this();
        this.description = description;
    }

    /**
     * Paint Event<br>
     * Draws the whole gauge
     *
     * @param g graphics
     */
    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setFont(new Font("Open Sans", Font.PLAIN, 20));
        g2.setPaint(backgroundColor);
        g2.fillRect(0, 0, getWidth(), getHeight());
        drawChart(g2);
        drawDescription(g2);
        drawValue(g2);
        drawMinValue(g2);
        drawMaxValue(g2);
        drawUnit(g2);
    }

    /**
     * Draws donut
     *
     * @param g graphics
     */
    private void drawChart(Graphics2D g) {
        float percent = percentValue();
        radius = (getWidth() - (margin * 2)) / 2;

        //Draw background donut
        g.setPaint(gaugeBackgroundColor);
        drawHalfDonut(g, margin, margin, radius, 1);

        //Draw value donut
        g.setPaint(foregroundColor.getFadedColor(percent));
        drawHalfDonut(g, margin, margin, radius, percent);

        //Draw Circle
        g.setPaint(backgroundColor);
        drawCircle(g, margin + radius, margin + radius, radius - (padding * 2));
    }

    /**
     * Draws description label
     *
     * @param g graphics
     */
    private void drawDescription(Graphics2D g) {
        int textPadding = 20;
        float textHeight = (radius - padding) / 6;

        g.setPaint(textColor);
        g.setFont(getFontWithHeight(g.getFont(), textHeight));
        drawCenteredText(g, margin + radius, margin + radius + textPadding + (int) textHeight, description);
    }

    /**
     * Draws current value label
     *
     * @param g graphics
     */
    private void drawValue(Graphics2D g) {
        g.setPaint(textColor);
        g.setFont(getFontWithHeight(g.getFont(), (radius - padding) / 2));
        drawCenteredText(g, margin + radius, margin + radius, String.format("%d", currentValue));
    }

    /**
     * Draws min value label
     *
     * @param g graphics
     */
    private void drawMinValue(Graphics2D g) {
        int textPadding = 5;
        float textHeight = padding / 2;

        g.setPaint(textColor);
        g.setFont(getFontWithHeight(g.getFont(), textHeight));
        drawCenteredText(g, margin + padding, margin + radius + textPadding + (int) textHeight, String.format("%d", minValue));
    }

    /**
     * Draws max value label
     *
     * @param g graphics
     */
    private void drawMaxValue(Graphics2D g) {
        int textPadding = 5;
        float textHeight = padding / 2;

        g.setPaint(textColor);
        g.setFont(getFontWithHeight(g.getFont(), textHeight));
        drawCenteredText(g, margin + (radius * 2) - padding, margin + radius + textPadding + (int) textHeight, String.format("%d", maxValue));
    }

    /**
     * Draws unit label
     *
     * @param g graphics
     */
    private void drawUnit(Graphics2D g) {
        int textPadding = 8;
        float textHeight = padding / 1.5f;

        g.setPaint(textColor);
        g.setFont(getFontWithHeight(g.getFont(), textHeight));
        drawCenteredText(g, margin + radius, (margin + radius) - ((radius - padding) / 2) - textPadding, unit);
    }

    /**
     * Draws centered circle
     *
     * @param g graphics
     * @param x center
     * @param y center
     * @param radius radius
     */
    private void drawCircle(Graphics2D g, int x, int y, int radius) {
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }

    /**
     * Draws half donut
     *
     * @param g graphics
     * @param x center
     * @param y center
     * @param radius radius
     * @param percent filled
     */
    private void drawHalfDonut(Graphics2D g, int x, int y, int radius, double percent) {
        g.fill(new Arc2D.Double(x, y, radius * 2, radius * 2, 180, -(180 * percent), Arc2D.PIE));
    }

    /**
     * Draw centered text
     *
     * @param g graphics
     * @param x center
     * @param y center
     * @param text text to draw
     */
    private void drawCenteredText(Graphics2D g, int x, int y, String text) {
        FontMetrics fm = g.getFontMetrics();
        int width = fm.stringWidth(text);
        g.drawString(text, x - (width / 2), y);
    }

    /**
     * Get font with size that fights into height
     *
     * @param f default font
     * @param height height bound
     * @return new font
     */
    private Font getFontWithHeight(Font f, float height) {
        float size = height;
        boolean up = false;
        while (true) {
            Font font = f.deriveFont(size);
            int testHeight = getFontMetrics(font).getHeight();
            if (testHeight < height && up) {
                size += 0.1f;
                up = true;
            } else if (testHeight > height && !up) {
                size -= 0.1f;
                up = false;
            } else {
                return font;
            }
        }
    }

    /**
     * Process the position from the mouse and calculate value
     *
     * @param x mouse x
     * @param y mouse y
     */
    private void processValue(int x, int y) {
        if (enabled) {
            int zero = margin;
            int hundred = radius * 2;
            if (x >= zero || x <= hundred) {
                float percent = ((float) x - (float) zero) / (float) hundred;
                if (percent >= 0f && percent <= 1f) {
                    float interval = maxValue - minValue;
                    float valueWithoutMin = interval * percent;
                    float value = minValue + valueWithoutMin;
                    setCurrentValue((int) value);
                }
            }
        }
    }

    /**
     * Calculate percentage for the gauge
     *
     * @return percent
     */
    public float percentValue() {
        float interval = maxValue - minValue;
        float valueWithoutMin = currentValue - minValue;
        float p = valueWithoutMin / interval;

        if (p >= 0f && p <= 1f) {
            return p;
        }
        return 0;
    }

    /**
     * Action Listener when value is changed
     *
     * @param action event
     */
    public void addValueChangedListener(ActionListener action) {
        this.valueChanged = action;
    }

    /**
     * Get description text
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description text
     *
     * @param description text
     */
    public void setDescription(String description) {
        this.description = description;
        repaint();
    }

    /**
     * Get unit
     *
     * @return unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets unit
     *
     * @param unit unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
        repaint();
    }

    /**
     * Get minimum value
     *
     * @return value
     */
    public Integer getMinValue() {
        return minValue;
    }

    /**
     * Set minimum value
     *
     * @param minValue value
     */
    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
        repaint();
    }

    /**
     * Get maximum value
     *
     * @return value
     */
    public Integer getMaxValue() {
        return maxValue;
    }

    /**
     * Set maximum value
     *
     * @param maxValue value
     */
    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
        repaint();
    }

    /**
     * Get current value
     *
     * @return value
     */
    public Integer getCurrentValue() {
        return currentValue;
    }

    /**
     * Set current value
     *
     * @param currentValue valuerrent
     */
    public void setCurrentValue(Integer currentValue) {
        if (currentValue >= minValue && currentValue <= maxValue) {
            this.currentValue = currentValue;
        }
        if (valueChanged != null) {
            valueChanged.actionPerformed(new ActionEvent(this, 0, "valueChanged"));
        }
        repaint();
    }

    /**
     * Get text color
     *
     * @return color
     */
    public Color getTextColor() {
        return textColor;
    }

    /**
     * Set text color
     *
     * @param textColor color
     */
    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    /**
     * Get foreground color
     *
     * @return color
     */
    public ColorFade getForegroundColor() {
        return foregroundColor;
    }

    /**
     * Set foreground color
     *
     * @param foregroundColor color
     */
    public void setForegroundColor(ColorFade foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    /**
     * Get background color
     *
     * @return color
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Set background color
     *
     * @param backgroundColor color
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Get gauge background color
     *
     * @return color
     */
    public Color getGaugeBackgroundColor() {
        return gaugeBackgroundColor;
    }

    /**
     * Set gauge background color
     *
     * @param gaugeBackgroundColor color
     */
    public void setGaugeBackgroundColor(Color gaugeBackgroundColor) {
        this.gaugeBackgroundColor = gaugeBackgroundColor;
    }

    /**
     * Get padding
     *
     * @return padding
     */
    public Integer getPadding() {
        return padding;
    }

    /**
     * Set padding
     *
     * @param padding padding
     */
    public void setPadding(Integer padding) {
        this.padding = padding;
    }

    /**
     * Get margin
     *
     * @return margin
     */
    public Integer getMargin() {
        return margin;
    }

    /**
     * Set margin
     *
     * @param margin margin
     */
    public void setMargin(Integer margin) {
        this.margin = margin;
    }

    /**
     * Is enabled
     *
     * @return enabled
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Get enabled
     *
     * @param enabled value
     */
    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Mouse Event: dragged
     *
     * @param e event
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        processValue(e.getX(), e.getY());
    }

    /**
     * Mouse Event: moved, not needed
     *
     * @param e event
     */
    @Override
    public void mouseMoved(MouseEvent e) {
    }

    /**
     * Mouse Event: clicked, not needed
     *
     * @param e event
     */
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Mouse Event: pressed
     *
     * @param e event
     */
    @Override
    public void mousePressed(MouseEvent e) {
        processValue(e.getX(), e.getY());
    }

    /**
     * Mouse Event: released, not needed
     *
     * @param e event
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Mouse Event: entered, not needed
     *
     * @param e event
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Mouse Event: exited, not needed
     *
     * @param e event
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }

}
