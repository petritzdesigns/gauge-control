package at.petritzdesigns.gauge.utility;

import java.awt.Color;

/**
 * GaugeControl
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public interface ColorFade {

    /**
     * Get Faded color
     * @param percent value
     * @return color
     */
    public Color getFadedColor(float percent);
}
