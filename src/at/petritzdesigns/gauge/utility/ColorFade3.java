package at.petritzdesigns.gauge.utility;

import java.awt.Color;

/**
 * BitBotControl
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ColorFade3 implements ColorFade {

    /**
     * First Color
     */
    private final Color color1;

    /**
     * Second Color
     */
    private final Color color2;

    /**
     * Third Color
     */
    private final Color color3;

    /**
     * Default Constructor
     *
     * @param color1 first color
     * @param color2 second color
     * @param color3 third color
     */
    public ColorFade3(Color color1, Color color2, Color color3) {
        this.color1 = color1;
        this.color2 = color2;
        this.color3 = color3;
    }

    /**
     * Constructor with no color fade
     *
     * @param color color
     */
    public ColorFade3(Color color) {
        this.color1 = color;
        this.color2 = color;
        this.color3 = color;
    }

    /**
     * Returns interpolated color
     *
     * @param percent from 0.0 - 1.0
     * @return faded color
     */
    @Override
    public Color getFadedColor(float percent) {
        if (color1.equals(color2) && color2.equals(color3)) {
            return color1;
        }
        
        Color faded;
        if (percent <= 0.5f) {
            float red = (color2.getRed() / 255f) * percent * 2.0f + (color1.getRed() / 255f) * (0.5f - percent) * 2.0f;
            float green = (color2.getGreen() / 255f) * percent * 2.0f + (color1.getGreen() / 255f) * (0.5f - percent) * 2.0f;
            float blue = (color2.getBlue() / 255f) * percent * 2.0f + (color1.getBlue() / 255f) * (0.5f - percent) * 2.0f;
            faded = new Color(red, green, blue);
        } else {
            float red = (color3.getRed() / 255f) * (percent - 0.5f) * 2.0f + (color2.getRed() / 255f) * (1.0f - percent) * 2.0f;
            float green = (color3.getGreen()/ 255f) * (percent - 0.5f) * 2.0f + (color2.getGreen() / 255f) * (1.0f - percent) * 2.0f;
            float blue = (color3.getBlue()/ 255f) * (percent - 0.5f) * 2.0f + (color2.getBlue() / 255f) * (1.0f - percent) * 2.0f;
            faded = new Color(red, green, blue);
        }
        return faded;
    }

    /**
     * Returns first color
     *
     * @return color
     */
    public Color getColor1() {
        return color1;
    }

    /**
     * Returns second color
     *
     * @return color
     */
    public Color getColor2() {
        return color2;
    }

    /**
     * Returns third color
     *
     * @return color
     */
    public Color getColor3() {
        return color3;
    }
}
