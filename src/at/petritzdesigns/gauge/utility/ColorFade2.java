package at.petritzdesigns.gauge.utility;

import java.awt.Color;

/**
 * GaugeControl
 * 
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ColorFade2 implements ColorFade {
    
    /**
     * First Color
     */
    private final Color color1;

    /**
     * Second Color
     */
    private final Color color2;


    /**
     * Default Constructor
     *
     * @param color1 first color
     * @param color2 second color
     */
    public ColorFade2(Color color1, Color color2) {
        this.color1 = color1;
        this.color2 = color2;
    }

    /**
     * Constructor with no color fade
     *
     * @param color color
     */
    public ColorFade2(Color color) {
        this.color1 = color;
        this.color2 = color;
    }

    /**
     * Returns interpolated color
     *
     * @param percent from 0.0 - 1.0
     * @return faded color
     */
    @Override
    public Color getFadedColor(float percent) {
        if (color1.equals(color2)) {
            return color1;
        }
        
        float red = (color1.getRed() / 255f) * percent + (color2.getRed() / 255f) * (1f - percent);
        float green = (color1.getBlue()/ 255f) * percent + (color2.getBlue() / 255f) * (1f - percent);
        float blue = (color1.getGreen()/ 255f) * percent + (color2.getGreen() / 255f) * (1f - percent);
        return new Color(red, green, blue);
    }

    /**
     * Returns first color
     *
     * @return color
     */
    public Color getColor1() {
        return color1;
    }

    /**
     * Returns second color
     *
     * @return color
     */
    public Color getColor2() {
        return color2;
    }
}
