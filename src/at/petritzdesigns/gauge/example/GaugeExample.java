package at.petritzdesigns.gauge.example;

import at.petritzdesigns.gauge.utility.ColorFade2;
import at.petritzdesigns.gauge.utility.ColorFade3;
import at.petritzdesigns.gauge.view.GaugeMeter;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * GaugeControl
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class GaugeExample extends JFrame {
    
    public GaugeExample() {
        initComponents();
    }
    
    private void initComponents() {
        setTitle("Gauge Example");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setLayout(new GridLayout(2, 2));

        GaugeMeter g1 = new GaugeMeter("Temperature");
        g1.setUnit("°C");
        g1.setMinValue(-14);
        g1.setMaxValue(30);
        g1.setCurrentValue(10);
        g1.setForegroundColor(new ColorFade3(Color.blue, Color.yellow, Color.red));
        g1.setPadding(40);
        g1.setMargin(40);
        
        GaugeMeter g2 = new GaugeMeter("Power");
        g2.setUnit("%");
        g2.setMinValue(0);
        g2.setMaxValue(100);
        g2.setCurrentValue(5);
        g2.setForegroundColor(new ColorFade2(Color.black, Color.green));
        g2.setPadding(50);
        g2.setMargin(50);
        
        GaugeMeter g3 = new GaugeMeter("GU-Power");
        g3.setCurrentValue(10);
        
        GaugeMeter g4 = new GaugeMeter("Test");
        g4.setPadding(50);
        
        add(g1);
        add(g2);
        add(g3);
        add(g4);
        
        pack();
        setSize(800, 600);
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GaugeExample ex = new GaugeExample();
                ex.setVisible(true);
            }
        });
    }
}
